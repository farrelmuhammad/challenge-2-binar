const isValidPassword = (password) => {
    const lowerCase = /[a-z]/g;
    const upperCase = /[A-Z]/g;
    const number = /[0-9]/g;

    if (password.match(lowerCase) && password.match(upperCase) && password.match(number) && password.length >= 8) {
        const hasil = console.log(true);
        return hasil;
    } else {
        const hasil = console.log(false);
        return hasil;
    }
}

isValidPassword('Meong2021')
isValidPassword('meong2021')
isValidPassword('@eong')
isValidPassword('Meong2')

// karena inputan bukan string tapu number
// isValidPassword(0)

// karena parameter tidak isi
// isValidPassword()