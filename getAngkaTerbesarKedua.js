const getAngkaTerbesarKedua = (array) => {
    const result = [...array];

    for (let i = 0; i < result.length; i++) {
        for (let j = i + 1; j < result.length; j++) {
            if (result[i] < result[j]) {
                let temp = result[i];
                result[i] = result[j];
                result[j] = temp;
            }
        }
    }

    const hasil = console.log(result[1]);
    return hasil;
}

const dataAngka = [9,4,7,7,4,3,2,2,8];

getAngkaTerbesarKedua(dataAngka)

// error karena input 0 tidak teriterasi loop
// getAngkaTerbesarKedua(0)

// karena tidak ada inputan di parameter
// getAngkaTerbesarKedua()