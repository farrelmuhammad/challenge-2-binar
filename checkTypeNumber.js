const checkTypeNumber = (givenNumber) => {
    if (typeof givenNumber === "number") {
        if (givenNumber % 2 === 0) {
            const hasil = console.log("GENAP");
            return hasil;
        } else {
            const hasil = console.log("GANJIL");
            return hasil;
        }
    } else {
        const hasil = console.log("Error: Invalid Data Type");
        return hasil;
    }
}


checkTypeNumber(10)
checkTypeNumber(3)
checkTypeNumber("3")
checkTypeNumber({})
checkTypeNumber([])
checkTypeNumber()
