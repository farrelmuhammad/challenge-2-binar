function changeWord (selectedText, changeText, text) {
    const textSplitted = text.split(' ');
    textSplitted.forEach((word, index) => {
      if (word === selectedText) {
        textSplitted[index] = changeText;
      }
    });
    return textSplitted.join(' ');
  }
  
  const kalimat1 = 'Andini sangat mencintai kamu selamanyaa';
  const kalimat2 = 'Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu';
  
  // changeWord('mencintai', 'membenci', kalimat1)
  
  console.log(changeWord('mencintai', 'membenci',kalimat1));
  console.log(changeWord('bromo', 'semeru', kalimat2));