const dataPenjualanNovel = [
    {
        idProduct: 'BOOK002421',
        namaProduk: 'Pulang - Pergi',
        penulis: 'Tere Liye',
        hargaBeli: 60000,
        hargaJual: 86000,
        totalTerjual: 150,
        sisaStok: 17,
    },
    {
        idProduct: 'BOOK002351',
        namaProduk: 'Selamat Tinggal',
        penulis: 'Tere Liye',
        hargaBeli: 75000,
        hargaJual: 103000,
        totalTerjual: 171,
        sisaStok: 20,
    },
    {
        idProduct: 'BOOK002941',
        namaProduk: 'Garis Waktu',
        penulis: 'Fiersa Besari',
        hargaBeli: 67000,
        hargaJual: 99000,
        totalTerjual: 213,
        sisaStok: 5,
    },
    {
        idProduct: 'BOOK002941',
        namaProduk: 'Laskar Pelangi',
        penulis: 'Andrea Hirata',
        hargaBeli: 55000,
        hargaJual: 68000,
        totalTerjual: 20,
        sisaStok: 56,
    },
];

const getInfoPenjualan = (dataPenjualanNovel) => {

    const result = []
    let stokTerjual = 0;
    let totalJual = 0;
    let stok = 0;
    let modal = 0;

    if (typeof dataPenjualanNovel === "object") {
        for (let i = 0; i < dataPenjualanNovel.length; i++) {
            stokTerjual += dataPenjualanNovel[i].totalTerjual;
            totalJual += dataPenjualanNovel[i].hargaJual;
            stok += dataPenjualanNovel[i].sisaStok;
            modal += dataPenjualanNovel[i].hargaBeli;
        }

        stok += stokTerjual;
        const totalKeuntungan = stokTerjual * totalJual;
        const totalModal = stok * modal;
        const presentaseKeuntungan = totalModal * 100 / totalKeuntungan;

        for (let j = 0; j < dataPenjualanNovel.length; j++) {
            for (let k = j + 1; k < dataPenjualanNovel.length; k++) {
                if (dataPenjualanNovel[j].totalTerjual < dataPenjualanNovel[k].totalTerjual) {
                    let temp = dataPenjualanNovel[j];
                    dataPenjualanNovel[j] = dataPenjualanNovel[k];
                    dataPenjualanNovel[k] = temp;
                }
            }
        }

        result.push({
            "totalKeuntungan": String(totalKeuntungan),
            "totalModal": String(totalModal),
            "presentaseKeuntungan": String(presentaseKeuntungan),
            "produkBukuTerlaris": dataPenjualanNovel[0].namaProduk,
            "penulisTerlaris": dataPenjualanNovel[0].penulis,
        });

        return result;
    }
}

console.log(getInfoPenjualan(dataPenjualanNovel));