function getSplitName(personNames) {
    var names = personNames.split(' ');
    if (names.length === 3) {
        output = [{firstName: names[0], middleName: names.slice(1, -1).join(' '), lastname: names[names.length - 1]}]; 
        return output;
      }
      else if (names.length < 2) {
        output = [{firstName: names[0], middleName: null, lastname: null}];
        return output;
      }
      else if (names.length === 2){
        output = [{firstName: names[0], middleName: null, lastname: names[names.length - 1]}];
        return output;
      }
      else {
        output = "Error : This function is only for 3 characters name";
        return output;
      }
}

console.log(getSplitName("Aldi Daniela Pranata"));
console.log(getSplitName("Dwi Kuncoro"));
console.log(getSplitName("Aurora"));
console.log(getSplitName("Aurora Aureliya Sukma Darma"));

// error karena inputan tidak ada iterasi dan tidak diproses, dan hanya menerima string bukan number
// console.log(getSplitName(0));