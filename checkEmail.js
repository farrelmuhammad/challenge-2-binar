const checkEmail = (email) => {
    const validRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (email.match(validRegex)) {
        const hasil = console.log("VALID")
        return hasil;
    } else {
        const hasil = console.log("INVALID");
        return hasil;
    }
}

checkEmail('apranata@binar.co.id')
checkEmail('apranata@binar.com')
checkEmail('apranata@binar')
checkEmail('apranata')

// error karena tidak ada function checkTypeNumber
// checkTypeNumber(checkEmail(3322)) 

// error karena parameter kosong
// checkEmail()